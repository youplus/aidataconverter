from tfs_calculation.calculate_tfs_nfs import tfs_main 
import pandas as pd

def filter_df(df, engagement=None, gender=None, age=None):

    engagement_map = {'h': 'high', 'l': 'low'}
    gender_map = {'m':'Male', 'f': 'Female'}
    age_map = {1: 'below 20', 2: '21-25', 3: '25-30', 4: '31 above' }


    if engagement is not None:
        if engagement in engagement_map:
            engagement = engagement_map[engagement]
            df = df[df['engagement']==engagement]
    
    if gender is not None:
        if gender in gender_map:
            gender = gender_map[gender]
            df = df[df['gender']==gender]


    if age is not None:
        if age in age_map:
            age = age_map[age]
            df = df[df['age_groups']==age]

    return df


def get_group_attribute_data(df, engagement=None, gender=None, age=None):
    df = filter_df(df, engagement, gender, age)
    df_aspect, df_group_aspect, df_product = tfs_main(df)
    return df_group_aspect


def get_attribute_data(df, group_attribute, engagement=None, gender=None, age=None):
    
    #print('DF Shape', df.shape)
    df = filter_df(df, engagement, gender, age)
    #print('Filteres Shape', df.shape)
    df_aspect, df_group_aspect, df_product = tfs_main(df)
    df_aspect = df_aspect[df_aspect['GroupAttribute']==group_attribute]
    return df_aspect

def get_product_data(df, engagement=None, gender=None, age=None):
    #print('DF Shape', df.shape)
    df = filter_df(df, engagement, gender, age)
    #print('Filteres Shape', df.shape)
    df_aspect, df_group_aspect, df_product = tfs_main(df)
    return df_product

import json
if __name__ == '__main__':
    path='data/li.csv'
    
    df = pd.read_csv(path)
    #print(df)
    
   


    #print(get_group_attribute_data(df))
    #get_attribute_data(df, group_attribute)
    #df_dict = list(df.T.to_dict().values())
    ##print(json.dumps(df_dict, indent=4))

