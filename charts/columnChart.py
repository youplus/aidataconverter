from pymongo import MongoClient
from bson.objectid import ObjectId

from util.db import DB



class ColumnChart:




  

    def insertBasicColumn(self,study_id,country,question_order_number, order_number,json ,ageGroup , gender, engagement,size,splineColor):
        obj = ObjectId()

        if json["spline_values"] and json["bar_values"]:

            data={
            "_id": obj,
            "study_id" : ObjectId(study_id),
            "country" : country,
            "product_type" : "study_country_question",
            "approved" : True,
            "published" :True,
            "order_number" : order_number,
            "width" : size,# 12 for non charts, 6 for charts type
            "config" : {
                "series" : ([
                    {
                        "type" : "column",
                        "colorByPoint" : True,
                        "data" : json["bar_values"],
                        "marker" : {
                            "fillColor" : ""
                        },
                        "name" : "Volumetric",
                        "yAxis" : 0.0
                    },
                    
                    {
                        "type" : "spline",
                        "colorByPoint" : False,
                        "color":splineColor,
                        "data" : json["spline_values"],
                        "marker" : {
                            "fillColor" : ""
                        },
                        "name" : "TFS",
                        "yAxis" : 1.0 ,
                        "showEmpty": False
                    },
                ]),
                "xAxis" : {
                    "labels" : {
                        "style" : {
                            "color" : "#fff",
                            "fontFamily" : "Helvetica Neue, sans-serif",
                            "fontSize" : 12.0
                        }
                    },
                    "lineColor" : "#666666",
                    "type" : "category"
                },
                "plotOptions" : {
                        "column":{
                            "dataLabels":{
                            "format":"{y}%"
                            },
                            "showInLegend":True,
                            "dataSorting":{
                            "enabled":True,
                            }
                        },
                        "line":{
                            "dataLabels":{
                            "format":"{y}"
                            }
                        },
                        "pie":{
                            "dataLabels":{
                            "distance":-50,
                            "filter":{
                                "operator":">",
                                "property":"percentage",
                                "value":4
                            },
                            "format":"<br>{point.percentage:.1f} %"
                            },
                            "showInLegend":True
                        },
                        "series":{
                            "allowPointSelect":True,
                            "borderColor":"transparent",
                            "cursor":"pointer",
                            "dataLabels":{
                            "color":"#fff",
                            "enabled":True,
                            "style":{
                                "color":"#fff",
                                "fontSize":"14"
                            }
                            }
                        },
                        "spline":{
                            "dataLabels":{
                            "format":"{y}"
                            }
                        }
                        },
                "credits" : False,
                "legend" : {
                    "enabled" : False
                },
                "subtitle" : {
                    "align" : "left",
                    "style" : {
                        "color" : "#7A7C87",
                        "fontFamily" : "Montserrat",
                        "fontSize" : "1.1em",
                        "fontWeight" : "400",
                        "letterSpacing" : "0.05em"
                    },
                    "text" : ""
                },
                "title" : {
                    "align" : "left",
                    "style" : {
                        "color" : "#D3D3D7",
                        "fontFamily" : "Montserrat",
                        "fontSize" : "1.6em",
                        "fontWeight" : 400.0
                    },
                    "text" : json["Title"]
                },
                "yAxis" : [
                    {
                        "gridLineWidth" : 0.0,
                        "index" : 0.0,
                        "lineColor" : "#666666",
                        "lineWidth" : 1.0,

                        "title" : {
                            "text" : "Volumetric",
                                        "style": {
                                    "color": 'white'
                }
                        },
                        "min":0,
                        "max":100,
                    },
                    {
                    "title" : {
                        "text" : "TFS"
                    },
                    "gridLineWidth" : 0.0,
                    "index" : 1.0,
                    "lineColor" : "#666666",
                    "lineWidth" : 1.0,
                    "opposite" : True,
                    "showEmpty" : False,
                    "min":0,
                    "max":10,
        
                }
                ],
                "chart" : {
                    "plotShadow" : False,
                    "style" : {
                        "fontFamily" : "Helvetica Neue, sans-serif",
                        "fontWeight" : "bold"
                    },
                    "type" : "column",
                    "backgroundColor" : "#222437",
                    "height" : "405px",
                    "plotBackgroundColor" : "#222437",
                    "plotBorderWidth" : None
                }
            },

            "question_order_number" : question_order_number,
            "insights" : [], # insights
            "section" : "",# section
            "verbatims" : [],# verbatims
            "html" : "",# html
            "type" : "charts",# charts/html/section/verbatims/insights/
            "chart_type" : "COLUMN", #pie/column_with_spline/column/spline/bar  - when type is charts
            "test2" : "test2",
            "ageGroup" : ageGroup,
            "gender":gender, 
            "engagement":engagement
            
        }

        elif json["bar_values"]:
            data =     data={
            "_id": obj,
            "study_id" : ObjectId(study_id),
            "country" : country,
            "product_type" : "study_country_question",
            "approved" : True,
            "published" :True,
            "order_number" : order_number,
            "width" : size,# 12 for non charts, 6 for charts type
            "config" : {
                "series" : ([
                    {
                        "type" : "column",
                        "colorByPoint" : True,
                        "data" : json["bar_values"],
                        "marker" : {
                            "fillColor" : ""
                        },
                        "name" : "Volumetric",
                        "yAxis" : 0.0
                    }
                ]),
                "xAxis" : {
                    "labels" : {
                        "style" : {
                            "color" : "#fff",
                            "fontFamily" : "Helvetica Neue, sans-serif",
                            "fontSize" : 12.0
                        }
                    },
                    "lineColor" : "#666666",
                    "type" : "category"
                },
                "plotOptions" : {
                        "column":{
                            "dataLabels":{
                            "format":"{y}%"
                            },
                            "showInLegend":True
                        },
                        "line":{
                            "dataLabels":{
                            "format":"{y}"
                            }
                        },
                        "pie":{
                            "dataLabels":{
                            "distance":-50,
                            "filter":{
                                "operator":">",
                                "property":"percentage",
                                "value":4
                            },
                            "format":"<br>{point.percentage:.1f} %"
                            },
                            "showInLegend":True
                        },
                        "series":{
                            "allowPointSelect":True,
                            "borderColor":"transparent",
                            "cursor":"pointer",
                            "dataLabels":{
                            "color":"#fff",
                            "enabled":True,
                            "style":{
                                "color":"#fff",
                                "fontSize":"14"
                            }
                            }
                        },
                        "spline":{
                            "dataLabels":{
                            "format":"{y}"
                            }
                        }
                        },
                "credits" : False,
                "legend" : {
                    "enabled" : False
                },
                "subtitle" : {
                    "align" : "left",
                    "style" : {
                        "color" : "#7A7C87",
                        "fontFamily" : "Montserrat",
                        "fontSize" : "1.1em",
                        "fontWeight" : "400",
                        "letterSpacing" : "0.05em"
                    },
                    "text" : ""
                },
                "title" : {
                    "align" : "left",
                    "style" : {
                        "color" : "#D3D3D7",
                        "fontFamily" : "Montserrat",
                        "fontSize" : "1.6em",
                        "fontWeight" : 400.0
                    },
                    "text" : json["Title"]
                },
                "yAxis" : [
                    {
                        "gridLineWidth" : 0.0,
                        "index" : 0.0,
                        "lineColor" : "#666666",
                        "lineWidth" : 1.0,

                        "title" : {
                            "text" : "Volumetric",
                                        "style": {
                                    "color": 'white'
                }
                        }
                    
                    ,   
                        "min":0,
                        "max":100,
                        "tickInterval":10,
                    }],
                "chart" : {
                    "plotShadow" : False,
                    "style" : {
                        "fontFamily" : "Helvetica Neue, sans-serif",
                        "fontWeight" : "bold"
                    },
                    "type" : "column",
                    "backgroundColor" : "#222437",
                    "height" : "405px",
                    "plotBackgroundColor" : "#222437",
                    "plotBorderWidth" : None
                }
            },

            "question_order_number" : question_order_number,
            "insights" : [], # insights
            "section" : "",# section
            "verbatims" : [],# verbatims
            "html" : "",# html
            "type" : "charts",# charts/html/section/verbatims/insights/
            "chart_type" : "COLUMN", #pie/column_with_spline/column/spline/bar  - when type is charts
            "test2" : "test2",
            "ageGroup" : ageGroup,
            "gender":gender, 
            "engagement":engagement
            
        }

        dbInstance = DB()
        dbInstance.insertChart(data)

  