

from bson import ObjectId
from pymongo import MongoClient
from bson.objectid import ObjectId
from util.db import DB
class SectionData:

    def insertSection(self,study_id,country,question_order_number, order_number,product_type,value,value2,ageGroup,gender,engagement):
        objId = ObjectId()
        print("insertHTMLChart")

        data={
            "_id":objId,
            "study_id" : ObjectId(study_id),
            "country" : country,
            "product_type" : "study_country_question",
            "attributes" : [],
            "group_attributes" : [],
            "approved" : True,
            "order_number" : order_number,
            "width" : 24,
            "config" : None,
            "default_config" : None,
            "question_order_number" : 1,
            "title" : "",
            "polarities" : [],
            "insights" : [],
            "section" : value,
            "verbatims" : [],
            "html" : "<span style=\"font-size: 9em; color: rgb(255, 153, 0);\" id=\""""+value2+"""\"></span>""",
            "type" : "section",
            "chart_contents" : "",
            "type_scores" : [],
            "type_volumes" : [],
            "chart_type" : "",
            "attribute_type" : "",
            "published" : True,
            "gender":gender,
            "ageGroup":ageGroup,
            "engagement":engagement

        }
                
        dbInstance = DB()
        dbInstance.insertChart(data)
        print("inserted successfully !!!")