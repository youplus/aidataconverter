from bson.objectid import ObjectId
from pymongo import MongoClient
from bson.objectid import ObjectId
from util.db import DB


class PieCh:

    def pieChartType(self):
        nameList = []
        data = {
        "Title": "GG wash Vs Machine wash",
        "subTitle": "",
        "Type": "Pie",
        "titleColor": "#50ade8",
        "Width": {
        "lg": 6,
        "xl": 6,
        "xs": 24
        },
        "Height": "444px",
        "values": [
        {
        "name": "Machine wash",
        "y":66,
        "color": "#50ade8",
        "sliced": ""
        },
        {
        "name": "Hand wash",
        "y": 34,
        "color": "#fbaf35",
        "sliced": ""
        }
        ]
        }

        title = data["Title"]
        for names in data["values"]:
            nameList.append(names)
    #     source_coll.insert_one(data)
        return nameList,title



    def insertPieChart(self,study_id,country,question_order_number, order_number,json ,ageGroup , gender, engagement):
        
        objId = ObjectId()

        data = {
    "_id" : objId,
    "study_id" : ObjectId(study_id),
    "country" : country,
    "product_type" : "study_country_question",
    "attributes" : [],
    "group_attributes" : [],
    "approved" : True,
    "published" :True,
    "order_number" : 1,
    "width" : 12,
    "config" : {
        "credits" : False,
        "series" : [ 
            {
                "data" : json["values"],
                "marker" : {
                    "fillColor" : ""
                },
                "name" : "Volumetric",
                "type" : "",
                "lineColor" : "",
                "yAxis" : 0.0,
                "colorByPoint" : True
            }
        ],
        "title" : {
            "style" : {
                "color" : "#D3D3D7",
                "fontFamily" : "Montserrat",
                "fontSize" : "1.6em",
                "fontWeight" : 400.0
            },
            "text" : json["Title"],
            "align" : "left"
        },
        "xAxis" : {
            "labels" : {
                "style" : {
                    "color" : "#fff",
                    "fontFamily" : "Helvetica Neue, sans-serif",
                    "fontSize" : 12.0
                }
            },
            "lineColor" : "#666666",
            "type" : "category"
        },
        "yAxis" : [ 
            {
                "max" : 100.0,
                "title" : {
                    "text" : "Volumetric"
                    
                },
                "opposite" : False,
                "index" : 0.0,
                "gridLineWidth" : 0.0,
                "lineColor" : "#666666",
                "lineWidth" : 1.0,
                "showEmpty" : False
            }, 
            {
                "showEmpty" : False,
                "max" : 10.0,
                "title" : {
                    "text" : "TFS"
                },
                "opposite" : True,
                "index" : 1.0,
                "gridLineWidth" : 0.0,
                "lineColor" : "#666666",
                "lineWidth" : 1.0
            }
        ],
        "chart" : {
            "backgroundColor" : "#222437",
            "height" : "405px",
            "plotBackgroundColor" : "#222437",
            "plotBorderWidth" : None,
            "plotShadow" : False,
            "style" : {
                "fontFamily" : "Helvetica Neue, sans-serif",
                "fontWeight" : "bold"
            },
            "type" : "pie"
        },
        "legend" : {
            "enabled" : True,
            "itemStyle": {"color": "#FFF"}
        },
        "plotOptions" : {
             "pie":{
                        "dataLabels":{
                        "distance":-50,
                        "filter":{
                            "operator":">",
                            "property":"percentage",
                            "value":4
                        },
                        "format":"<br>{point.percentage:.1f} %"
                        },
                        "showInLegend":True
                    },
            "column" : {
                "dataLabels" : {
                    "format" : "{y}%"
                },
                "showInLegend" : True
            },
            "spline" : {
                "dataLabels" : {
                    "format" : "{y}"
                }
            },
            "line" : {
                "dataLabels" : {
                    "format" : "{y}"
                }
            },
            "series" : {
                "allowPointSelect" : True,
                "borderColor" : "transparent",
                "cursor" : "pointer",
                "dataLabels" : {
                    "color" : "#fff",
                    "enabled" : True,
                    "style" : {
                        "fontSize" : "14",
                        "color" : "#fff"
                    }
                }
            }
        },
        "subtitle" : {
            "align" : "left",
            "style" : {
                "color" : "#7A7C87",
                "fontFamily" : "Montserrat",
                "fontSize" : "1.1em",
                "fontWeight" : "400",
                "letterSpacing" : "0.05em"
            },
            "text" : ""
        }
    },
    "default_config" : {
        "chart" : {
            "backgroundColor" : "#222437",
            "height" : "405px",
            "plotBackgroundColor" : "#222437",
            "plotBorderWidth" : None,
            "plotShadow" : False,
            "style" : {
                "fontFamily" : "Helvetica Neue, sans-serif",
                "fontWeight" : "bold"
            },
            "type" : "column"
        },
        "credits" : False,
        "legend" : {
            "enabled" : False
        },
        "plotOptions" : {
            "column" : {
                "dataLabels" : {
                    "format" : "{y}%"
                },
                "showInLegend" : True
            },
            "spline" : {
                "dataLabels" : {
                    "format" : "{y}"
                }
            },
            "line" : {
                "dataLabels" : {
                    "format" : "{y}"
                }
            },
            "series" : {
                "allowPointSelect" : True,
                "borderColor" : "transparent",
                "cursor" : "pointer",
                "dataLabels" : {
                    "color" : "#fff",
                    "enabled" : True,
                    "style" : {
                        "fontSize" : "14",
                        "color" : "#fff"
                    }
                }
            },
            "pie" : {
                "showInLegend" : True,
                "dataLabels" : {
                    "format" : "<br>{point.percentage:.1f} %",
                    "distance" : -50,
                    "filter" : {
                        "property" : "percentage",
                        "operator" : ">",
                        "value" : 4
                    }
                }
            }
        },
        "series" : [ 
            {
                "colorByPoint" : True,
                "data" : json["values"],
                "name" : "Volumetric",
                "type" : "column",
                "lineColor" : ""
            }, 
            {
                "data" : [ 
                    {
                        "name" : "Positive mentions",
                        "y" : 6.0,
                        "color" : "#50ade8"
                    }, 
                    {
                        "name" : "Negative mentions",
                        "y" : 2.0,
                        "color" : "#fbaf35"
                    }
                ],
                "name" : "TFS",
                "type" : "spline",
                "lineColor" : "#7FDCFE",
                "yAxis" : 1
            }
        ],
        "subtitle" : {
            "align" : "left",
            "style" : {
                "color" : "#7A7C87",
                "fontFamily" : "Montserrat",
                "fontSize" : "1.1em",
                "fontWeight" : "400",
                "letterSpacing" : "0.05em"
            },
            "text" : ""
        },
        "title" : {
            "align" : "left",
            "style" : {
                "color" : "#D3D3D7",
                "fontFamily" : "Montserrat",
                "fontSize" : "1.6em",
                "fontWeight" : 400
            },
            "text" : "Enter a New Title"
        },
        "xAxis" : {
            "labels" : {
                "style" : {
                    "color" : "#fff",
                    "fontFamily" : "Helvetica Neue, sans-serif",
                    "fontSize" : 12
                }
            },
            "lineColor" : "#666666",
            "type" : "category"
        },
        "yAxis" : [ 
            {
                "gridLineWidth" : 0,
                "lineColor" : "#666666",
                "lineWidth" : 1,
                "showEmpty" : False,
                "max" : 100,
                "title" : {
                    "text" : "Volumetric"
                }
            }, 
            {
                "gridLineWidth" : 0,
                "lineColor" : "#666666",
                "lineWidth" : 1,
                "showEmpty" : False,
                "max" : 10,
                "title" : {
                    "text" : "TFS"
                },
                "opposite" : True
            }
        ]
    },
    "question_order_number" : 1,
    "title" : "",
    "polarities" : [],
    "insights" : [],
    "section" : "",
    "verbatims" : [],
    "html" : "",
    "type" : "charts",
    "chart_contents" : "",
    "type_scores" : [],
    "type_volumes" : [],
    "chart_type" : "pie",
    "ageGroup" : ageGroup,
    "gender":gender, 
    "engagement":engagement
}

        

        dbInstance = DB()
        print(data)
        dbInstance.insertChart(data)
