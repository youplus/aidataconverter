from bson import ObjectId
from pymongo import MongoClient
from bson.objectid import ObjectId
from util.db import DB
class HTMLData:

    def insertHTMLChart(self,study_id,country,question_order_number, order_number,value,value2,ageGroup , gender, engagement,size):
        objId = ObjectId()
        #print("insertHTMLChart")
        data = {
    "_id":objId,
    "study_id" : ObjectId(study_id),
    "country" : country,
    "product_type" : "study_country_question",
    "approved" : True,
    "order_number" : order_number,
    "width" : size,
    "config" : None,
    "default_config" : None,
    "question_order_number" : 1,
    "title" : "",
    "html" : """<p><span style=\"font-size: 9em; color: rgb(255, 153, 0);\">"""+str(value)+"""</span><span style=\"font-size: 3.5em; color: rgb(255, 153, 0);\"></span><span style=\"font-size: 3.5em; color: rgb(136, 136, 136);\">
        <span class=\"ql-cursor\"></span></span></p><p><span style=\"font-size: 28px; color: rgb(136, 136, 136);\">"""+str(value2)+"""</span></p>""",
    "type" : "html",
    "chart_contents" : "",
    "type_scores" : [],
    "type_volumes" : [],
    "chart_type" : "",
    "attribute_type" : "",
    "published" : True,
            "ageGroup" : ageGroup,
        "gender":gender, 
        "engagement":engagement
}
        
        dbInstance = DB()
        dbInstance.insertChart(data)
        #print("inserted successfully !!!")

    def insertBlankChart(self,study_id,country,question_order_number, order_number,value,value2,ageGroup , gender, engagement,size):
        objId = ObjectId()
        #print("insertHTMLChart")
        data = {
    "_id":objId,
    "study_id" : ObjectId(study_id),
    "country" : country,
    "product_type" : "study_country_question",
    "approved" : True,
    "order_number" : order_number,
    "width" : size,
    "config" : None,
    "default_config" : None,
    "question_order_number" : 1,
    "title" : str(value).title(),
    "empty":True,
    "html" : """<p><span style=\"font-size: 3em; color: rgb(255, 255, 255);\"></span>No Data Found for """+value+"""<span style=\"font-size: 3.5em; color: rgb(255, 153, 0);\"></span><span style=\"font-size: 3.5em; color: rgb(136, 136, 136);\">
        <span class=\"ql-cursor\"></span></span></p><p><span style=\"font-size: 20px; color: rgb(136, 136, 136);\"></span></p>""",
    "type" : "html",
    "chart_contents" : "",
    "type_scores" : [],
    "type_volumes" : [],
    "chart_type" : "",
    "attribute_type" : "",
    "published" : True,
    "ageGroup" : ageGroup,
    "gender":gender, 
    "engagement":engagement
    
}
        
        dbInstance = DB()
        dbInstance.insertChart(data)
        #print("inserted successfully !!!")
