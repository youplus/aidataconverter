from bson.objectid import ObjectId
from pymongo import MongoClient


source_uri = "mongodb://youplus:youplus9091@ind-prod-mongos.yupl.us/youplus_production?authMechanism=SCRAM-SHA-1"
source_connection = MongoClient(source_uri)
source_db = source_connection['youplus_production']
source_coll = source_db['study_json']
graph = source_db['study_graph']
backup_graph=source_db['backup_graph']
youplus_q_tags = source_db['youplus_q_tags']
ordnum=0


class DB:

    def insertChart(self,data):
        data["migrate"] =True
        global ordnum
        data["order_number"] = ordnum
        ordnum = ordnum+1
        #backup_graph.insert_one(data)
        source_coll.insert_one(data)
        graph.insert_one(data)
        

        print("inserted successfully !!!")

    def insertVideo(self,data):

        
        data["script"] =True
        #print("data is")
        #print(data)
        
        #youplus_q_tags.insert_one(data)

        #print("inserted successfully !!!")

    def findVideo(self,docId):

        query = {"_id":ObjectId(docId)}
        mydoc = youplus_q_tags.find(query)
        return mydoc
    
    def update(self,idx,sureAttributes):

        query = { "_id": ObjectId(idx) }
        #print(sureAttributes)
        if(sureAttributes):
            for attribute in sureAttributes:
                attribute["group_attribute"] = "Mock Group Attribute"
                for record in attribute["intensity"]:
                    if record["polarity"] == "Neutral":
                        record["sentiment"]=1
                        record["polarity"]="Positive"
                        #print("converting neutral")
            
        newValues = { "$set": { "sure_attributes": sureAttributes , "script_absa":True} }
        youplus_q_tags.update_one(query,newValues)

