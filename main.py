import pandas as pd
import json
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import re
from charts import pieChart
from charts import columnChart
from charts import verbatims
from charts import insight
from charts import html
from charts import columnChartMultiple
from charts import section
from  tfs_calculation import tfs_service
from  tfs_calculation import calculate_tfs_nfs
from  tfs_calculation.tfs_service import filter_df
from  util import db
import copy
import math
import sys
import StudyGenerate
import glob


negative_color = "#f6af42"
positive_color ="#50ADE8"


male_color = "#50ADE8"
female_color ="#f6af42"

size_full = 24
size_half=12
size_quarter=6
size_three_quarter=18



filenamesMap = { 
   'body_cream':'5e1ca60a6dfe063d03f07b7e',
   'eyebrow_pencil':'5e1d751b6dfe060ed1c3033f',
   'hand_mask':'5e1d77c06dfe0612fbcf7d8b',
   'lotion':'5e1d87a96dfe063414af956c',
   'cleansing_lotion':'5e1ca4af6dfe06336b32ecf7',
   'cleanser':'5e1d8b7b6dfe063c6b8dfd63',
   'lip_balm':'5e1ca8ff167962494ae63cab',
   'face_wash':'5e1ca8286dfe0642e0e81f67',
   'smart_lights':'5e1ca6bc167962390cbba809',
   'perfumes':'5e1d78c36dfe0617a19782ab',
   'electric_razor':'5e1d76d86dfe060ed1c30f17',
   'nail_polish':'5e1d92196dfe064c068040cc',
   'eyeliner':'5e1ca93e6dfe0642e0e826d9',
   'hair_oil':'5e1ca5bd1679623556c0f6fd',
   'trimmer':'5e1d7aa56dfe061b58f363fc',
   'cleansing_wipes':'5e1ca78a6dfe06336b32fcf9',
   'hair_straighteners':'5e1d76eb6dfe0612fbcf7835',
   'hair_color':'5e1ca2876dfe06342ba4525e',
   'smart_home_security':'5e1ca4a7167962390cbb9b9b',
   'body_lotion':'5e1d8b956dfe063d66fa642c',
   'dental_care':'5e1ca5836dfe06336b32f24d',
   'exfoliating_scrub':'5e1ca2376dfe06336b32de5d',
   'laser_treatment':'5e1ca6ce1679623556c0fd07',
   'mask':'5e1d76446dfe0612fbcf731b',
   'foundations':'5e1d894d6dfe0638373a6911',
   'callus_remover':'5e1ca1bf6dfe06327558b396',
   'cleansing_gel':'5e1ca3a06dfe06336b32e7a1',
   'manual_razor':'5e1ca99a6dfe064b5735ae06',
   'pedicure':'5e1c3b9d6dfe06524228e12b',
   'tooth_paste':'5e1d7c0f6dfe061b58f36db8',
   'bridal_make_up':'5e1d734c6dfe060e5385daeb',
   'conditioner':'5e1d73aa6dfe060ed1c2fb91',
   'smart_vacuum_cleaners':'5e1d7dcf6dfe061b58f37af8',
   'hair_gel':'5e1d779f6dfe061411a2d275',
   'hair_styling':'5e1ca3041679623556c0ea53',
   'tooth_brush':'5e1c3b9c6dfe06524228e129',
   'hair_cut':'5e1d78916dfe061411a2d843',
   'mouth_wash':'5e1d7b1b6dfe061c76a69389',
   'foot_mask':'5e1d79bd6dfe061411a2e10f',
   'cuticle_oil':'5e1d77366dfe060ed1c31215',
   'after_shave_balmcream':'5e1d70df6dfe060b3093f83c',
   'eye_makeup_remover':'5e1ca2d76dfe06336b32e31d',
   'hair_treatment':'5e1d786d6dfe0612fbcf833b',
   'shaving_cream':'5e1ca6716dfe0638220c58a2',
   'hair_cream':'5e1ca6e56dfe0642e0e818c7',
   'base_coat':'5e1ca2966dfe06327558b964',
   'nail_art':'5e1ca4756dfe0638220c4d24',
   'lipstick':'5e1d88436dfe063414af9a4a',
   'facial':'5e1d76cf6dfe061411a2cc11',
   'floss':'5e1caae56dfe0642e0e82ee1',
   'nail_extension':'5e1d7beb6dfe061c76a699b1',
   'scrub':'5e1ca3746dfe0638220c471a',
   'shampoo':'5e1d796c6dfe0617a19787c5',
   'smart_speakers':'5e1d8b2d6dfe063c6b8dfb19',
   'waxing':'5e1d7cd26dfe061b58f3743a',
   'smart_kitchen_appliances':'5e1ca595167962390cbba0b5',
   'manicure':'5e1ca77b6dfe0638220c5eac',
   'mascara':'5e1d793e6dfe0612fbcf899f',
   'lip_gloss':'5e1d7a636dfe0612fbcf8f13',
   'eye_mask_under_eye_patches':'5e1d75c66dfe060ed1c30859',
   'hair_spray':'5e1ca3f51679623556c0efc7',
   'serum':'5e1d91236dfe0649fa61b28c',
   'cleansing_cream':'5e1ca4f46dfe063d03f075ce',
   'hair_extensions':'5e1d8e1d6dfe0642a27a5a53',
   'eye_shadow':'5e1d88fd6dfe06371e7d1cd5',
   'hair_serum':'5e1d78f46dfe061411a2dbf5',
   'shaving_gel':'5e1ca3b2167962390cbb9537',
   'top_coat':'5e1ca7ba167962390cbbadf5',
   "smart_thermostat":"5e2964736dfe06659b7dec56",
   "cleansing_foam":"5e2964b56dfe06659b7def01",
   "threading":"5e32a02f6dfe0631d97a215f"
   

      
      
}
class Analyser:
   df=None
   db=None
   ageFilter=None
   genderFilter=None
   engagementFilter=None
   titlePostString=None

   def __init__(self,filename):
      self.df = pd.read_csv("data/"+filename+".csv")

          

   def getProductNFS(self,study_id,country,question_order_number,order_number,product_type):
      htmlInstance = html.HTMLData()
      data={}
      
      resultDf = tfs_service.get_product_data(self.df,engagement=self.engagementFilter,age=self.ageFilter,gender=self.genderFilter)
      resultMap={}
      #print("-----------------")
      #print(resultDf)
      tfsScore= round(resultDf.to_dict()['Score']['TFS Score'],2)
   
      #print("-----------------")
      # for index, row in resultDf.iterrows():
      #   #print(----)
      #   #print(row)   

             
      htmlInstance.insertHTMLChart(study_id,country,question_order_number, order_number,tfsScore,"TFS",size_quarter)

   def insertBasicColumnWithDetails(self,titleChart,mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,ageFilter,genderFilter,engagementFilter,size,splineColor):
      #print(titleChart +"######################")
      if mapBarsList :   
         jsonInst= {}
         jsonInst["Title"] = titleChart + " " + self.titlePostString
         jsonInst["bar_values"]=mapBarsList
         jsonInst["spline_values"]=mapSplineList
         columnChartInstance = columnChart.ColumnChart()

         columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst ,self.ageFilter,self.genderFilter,self.engagementFilter,size,splineColor)
         #print("overall done")
      else:
            htmlInstance = html.HTMLData()
            value=titleChart
            #print("HTML Insert")
            htmlInstance.insertBlankChart(study_id,country,question_order_number, order_number,value,"",self.ageFilter,self.genderFilter,self.engagementFilter,size)

   def getOverallAttributes(self,study_id,country,question_order_number,order_number,product_type,file_name):
      self.df = pd.read_csv("data/"+file_name+".csv")
      #print(".....................")
      #print(self.engagementFilter)
      #print(self.ageFilter)
      #print(self.genderFilter)
      #print(".....................")
      #print(self.df)
      resultDf=  tfs_service.get_group_attribute_data(self.df,engagement=self.engagementFilter,age=self.ageFilter,gender=self.genderFilter)
      #print("overall.............")
      #print(resultDf)
      #print("**********************")
      #print("overall.............")

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_tfs","video_total_count","OVER")
      self.insertBasicColumnWithDetails("Overall Group Attributes",mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_full,positive_color)
      #print("overall done")

   def changeAgeName(self,ageNameIn):
      ageNameOut=ageNameIn
      ageYearsMap={'below 20':'below 20 years', '21-25': '21 - 25 years', '25-30': '25 - 30 years', '31 above': '31 years and above'}
      if ageNameIn in ageYearsMap.keys():
         ageNameOut = ageYearsMap[ageNameIn]
      return str(ageNameOut).title()
                      
                      
   def createBarAndSplineLists(self,resultDf,dataPointName,barValueName,splineValueName,videoCountName,flag):
          
      ageYearsMap={'below 20':'below 20 years', '21-25': '21 - 25 years', '25-30': '25 - 30 years', '31 above': '31 years and above'}
      mapBarsList = []
      mapSplineList=[]
      
      if not resultDf.size  == 0:

         rowSize = len(resultDf.index)
         columnSize = int(int(str(resultDf.size))/rowSize)
         numberOfVideos =  len(self.df)

         seriesValue=[]

         data=[]
      
         
         for index, row in resultDf.iterrows():
            #print(index)
            record=dict()
            record[dataPointName] = row[dataPointName]

            record[barValueName] =int(row[videoCountName]/numberOfVideos*100)

            record[splineValueName] =float(row[splineValueName])

            mapBar=dict()
            mapBar["name"]=str(record[dataPointName])
            if mapBar["name"] in ageYearsMap.keys():
                  mapBar["name"] = ageYearsMap[mapBar["name"]]
            mapBar["name"]=str(mapBar["name"]).title()      
            mapBar["y"]=round(record[barValueName],1)

            if not mapBar["y"] == 0:
                  
               if mapBar["y"] >= 0:
                  mapBar["color"]=positive_color
               else:
                  mapBar["y"]=mapBar["y"]*-1
                  mapBar["color"]=negative_color
              


               mapSpline=dict()
               mapSpline["name"]=str(record[dataPointName])
               if mapSpline["name"] in ageYearsMap.keys():
                  mapSpline["name"] = ageYearsMap[mapSpline["name"]]
               mapSpline["name"]=str(mapSpline["name"]).title()
               
               mapSpline["y"]=round(record[splineValueName],2)

               mapSpline["color"]=positive_color

               
               if flag  == "NEG" or  flag  == "OVER_NEG":
                  mapBar["color"]=negative_color
                  mapSpline["y"]=mapSpline["y"]*-1
                  mapSpline["color"]=negative_color

               if flag  == "OVER_NEG":
                  mapBar["color"]=negative_color
                  mapSpline["y"]=mapSpline["y"]*-1
               
               if flag  == "OVER":
                  if mapSpline["y"] < 0:
                     mapBar["color"]=negative_color
                     mapSpline["y"]=mapSpline["y"]*-1
               
               mapSplineList.append(mapSpline)
               mapBarsList.append(mapBar)

      return mapBarsList, mapSplineList
          

   def getOverallLikability(self,study_id,country,question_order_number,order_number,product_type,file_name,):
      self.df = pd.read_csv("data/"+file_name+".csv")

      resultDf=  tfs_service.get_group_attribute_data(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_positive_tfs","video_pos_count","POS")
      self.insertBasicColumnWithDetails("Positive Overall Group Attributes",mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,positive_color)
   

   def getOverallDislikes(self,study_id,country,question_order_number,order_number,product_type,file_name):
      self.df = pd.read_csv("data/"+file_name+".csv")

      resultDf=  tfs_service.get_group_attribute_data(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
      

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_negative_tfs","video_neg_count","NEG")
      self.insertBasicColumnWithDetails("Negative Overall Group Attributes",mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,negative_color)
   

   def getBreakoutsForAttributeLikedislike(self,study_id,country,question_order_number,order_number,product_type,file_name , groupAttributes):


      for ga in groupAttributes:
         #print("GA is__________________")
         #print(ga)
         #print("GA End")
         #print("for Postive group attribute .......")
         #print(ga)

         dfa = pd.read_csv("data/"+file_name+".csv")
         resultDf=  tfs_service.get_attribute_data(dfa,str(ga),engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
         #print(resultDf)
         mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"Attribute","Attribute_volumetric","Attribute_positive_tfs","video_pos_count","POS")
         #print("End Postive group attribute .......")
         #print("for Negative group attribute .......")
         #print(ga)
         dfa1 = pd.read_csv("data/"+file_name+".csv")
         resultDf1=  tfs_service.get_attribute_data(dfa1,str(ga),engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
         #print(resultDf1)
         mapBarsList1,mapSplineList1=self.createBarAndSplineLists(resultDf1,"Attribute","Attribute_volumetric","Attribute_negative_tfs","video_neg_count","NEG")
         #print("End Negative group attribute .......")         
         if  (mapBarsList or mapBarsList1):
            #print("The or condition")
            self.insertBasicColumnWithDetails("Positive Breakout For Attribute - " +ga.title(),mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,positive_color)

            self.insertBasicColumnWithDetails("Negative Breakout For Attribute - " +ga.title(),mapBarsList1,mapSplineList1,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,negative_color)
            #print("End the or condition")
   

   def getBreakoutsForAttributeDisliked(self,study_id,country,question_order_number,order_number,product_type,file_name , groupAttributes):
         for ga in groupAttributes:
            #print("for group attribute .......", ga)
            dfa = pd.read_csv("data/"+file_name+".csv")
            resultDf=  tfs_service.get_attribute_data(dfa,str(ga))
            
            mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"Attribute","Attribute_volumetric","Attribute_negative_tfs","video_neg_count","NEG")
            self.insertBasicColumnWithDetails("Negative Breakout For Attribute - " +ga.title(),mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,negative_color)
   

   def getHighEngagmentOverall(self,study_id,country,question_order_number,order_number,product_type,file_name):
          
      self.df = pd.read_csv("data/"+file_name+".csv")
      
      if self.engagementFilter in ["l"]:
         mapBarsList=[]
         mapSplineList=[]
      else:
         resultDf=  tfs_service.get_group_attribute_data(self.df,engagement="h",gender=self.genderFilter,age=self.ageFilter)

         mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_positive_tfs","video_pos_count","POS")
      self.insertBasicColumnWithDetails("High Engagement Level - Overall Group attributes",mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,positive_color)
      
      #print("High Engagement overall done")

   def getLowEngagementOverall(self,study_id,country,question_order_number,order_number,product_type,file_name):
      self.df = pd.read_csv("data/"+file_name+".csv")
      if self.engagementFilter in ["h"]:
         mapBarsList=[]
         mapSplineList=[]
      else:
         resultDf=  tfs_service.get_group_attribute_data(self.df,engagement="l",gender=self.genderFilter,age=self.ageFilter)
         mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_negative_tfs","video_neg_count","NEG")
      self.insertBasicColumnWithDetails("Low Engagement Level - Overall Group attributes",mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,negative_color)
   
      #print("Low Engagement overall done")

   def getGroupAttributes(self,file_name):
      groupAttributesList=[]
      self.df = pd.read_csv("data/"+file_name+".csv")
      print("filter is *****************************************")
      print(self.ageFilter ,self.genderFilter,self.engagementFilter )
      
      
      resultDf=  tfs_service.get_group_attribute_data(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)

      rowSize = len(resultDf.index)
      if rowSize !=0:
         columnSize = int(int(str(resultDf.size))/rowSize)
         numberOfVideos =  len(self.df)
         #print("column size:", columnSize)
         #print("rowSize:" ,rowSize)
         
         for i in range (0,rowSize):
            groupAttributesList.append(resultDf["GroupAttribute"][i])
      return  groupAttributesList
      

   def getGenderSplitEngagement(self,study_id,country,question_order_number,order_number,product_type,filename):
      fdf =filter_df(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
      if not fdf.empty:
         result = fdf["gender"].value_counts()

         pieChartInstance = pieChart.PieCh()
         pieValueList = [] 
         male=0
         if "Male" in result:
               male= result["Male"]
         female=0
         if "Female" in result:
            female= result["Female"]
         total = male+ female
         pieValueM={}
         pieValueM["name"]="Male"
         pieValueM["y"]=male*100/total
         pieValueM["color"]=male_color

         pieValueList.append(pieValueM)

         pieValueF={}
         pieValueF["name"]="Female"
         pieValueF["y"]=female*100/total
         pieValueF["color"]=female_color
         
         pieValueList.append(pieValueF)

         jsonInst= {}
         jsonInst["Title"] = "Engagement Level Gender Split - Content Creators "+self.titlePostString
         jsonInst["values"]=pieValueList

         

         pieChartInstance = pieChart.PieCh()

         pieChartInstance.insertPieChart(study_id,country,question_order_number, order_number,jsonInst,self.ageFilter,self.genderFilter,self.engagementFilter)
      else:
         htmlInstance = html.HTMLData()
         value="Engagement Level Gender Split - Content Creators "+self.titlePostString
         #print("HTML Insert")
         htmlInstance.insertBlankChart(study_id,country,question_order_number, order_number,value,"",self.ageFilter,self.genderFilter,self.engagementFilter,size_half)
      

   def getAgeGroupContentCreator(self,study_id,country,question_order_number,order_number,product_type,filename):
      fdf =filter_df(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
      
      
      mapBarsList = []
      total=0
      for items in fdf["age_groups"].value_counts().iteritems():
         total=total+items[1]
      for items in fdf["age_groups"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=self.changeAgeName(items[0])
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=positive_color
         mapBarsList.append(mapBar)

         



      title = "Engagement Level - Age Wise Split of Content Creators"

      mapSplineList=[]
      columnChartInstance = columnChart.ColumnChart()
      self.insertBasicColumnWithDetails(title,mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,positive_color)
           

   def getLowEngagementGenderSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      fdf =filter_df(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
      resultDf = pd.DataFrame(columns=["Empty"])
      resultDf = resultDf.fillna(0)
      if not fdf.empty:
         resultDf = fdf.loc[self.df['engagement'] == 'low']
      if not (fdf.empty or resultDf.empty or self.engagementFilter in ['h']):
        
         pieValueList = []
         total=0
         for items in resultDf["gender"].value_counts().iteritems():
            total=total+items[1]
         for items in resultDf["gender"].value_counts().iteritems(): 

            pieValue=dict()
            pieValue["name"]=items[0]
            pieValue["y"]=round((int(items[1]*100)/int(total)))
            if pieValue["name"] == "Male":
               pieValue["color"]=male_color
            if pieValue["name"] == "Female":
               pieValue["color"]=female_color
            pieValueList.append(pieValue)


         jsonInst= {}
         jsonInst["Title"] = "Low Engagement Level - Gender Wise Split of Content Creators " + self.titlePostString
         jsonInst["values"]=pieValueList

         pieChartInstance = pieChart.PieCh()


         pieChartInstance.insertPieChart(study_id,country,question_order_number, order_number,jsonInst,self.ageFilter,self.genderFilter,self.engagementFilter)
      else:
         htmlInstance = html.HTMLData()
         value="Low Engagement Level - Gender Wise Split of Content Creators "
         #print("HTML Insert")
         htmlInstance.insertBlankChart(study_id,country,question_order_number, order_number,value,"",self.ageFilter,self.genderFilter,self.engagementFilter,size_half)
      

   def getHighEngagementGenderSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      fdf =filter_df(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
      resultDf = pd.DataFrame(columns=["Empty"])
      resultDf = resultDf.fillna(0)
      if not fdf.empty:
         resultDf = fdf.loc[self.df['engagement'] == 'high']
      if not (fdf.empty or resultDf.empty or self.engagementFilter in ['l']):
         pieValueList = []
         total=0
         for items in resultDf["gender"].value_counts().iteritems():
            total=total+items[1]
         for items in resultDf["gender"].value_counts().iteritems(): 

            pieValue=dict()
            pieValue["name"]=items[0]
            pieValue["y"]=round((int(items[1]*100)/int(total)))
            if pieValue["name"] == "Male":
               pieValue["color"]=male_color
            if pieValue["name"] == "Female":
               pieValue["color"]=female_color

            pieValueList.append(pieValue)


         jsonInst= {}
         jsonInst["Title"] = "High Engagement Level - Gender Wise Split of Content Creators "+self.titlePostString
         jsonInst["values"]=pieValueList

         pieChartInstance = pieChart.PieCh()

         pieChartInstance.insertPieChart(study_id,country,question_order_number, order_number,jsonInst,self.ageFilter,self.genderFilter,self.engagementFilter)
      else:
         htmlInstance = html.HTMLData()
         value="High Engagement Level - Gender Wise Split of Content Creators "
      #print("HTML Insert")
         htmlInstance.insertBlankChart(study_id,country,question_order_number, order_number,value,"",self.ageFilter,self.genderFilter,self.engagementFilter,size_half)
      

   def getHighEngagementAgeSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      fdf =filter_df(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
      resultDf = fdf[fdf['engagement'] == 'high']
      mapBarsList = []
      total=0
      for items in resultDf["age_groups"].value_counts().iteritems():
       total=total+items[1]
      for items in resultDf["age_groups"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=self.changeAgeName(items[0])
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=positive_color
         mapBarsList.append(mapBar)


      
      title = "High Engagement Level - Age Wise Split of Content Creators "
 

      mapSplineList=[]
      self.insertBasicColumnWithDetails(title,mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,positive_color)
        

   def getLowEngagementAgeSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      fdf =filter_df(self.df,engagement=self.engagementFilter,gender=self.genderFilter,age=self.ageFilter)
      resultDf = fdf[fdf['engagement'] == 'low']
      mapBarsList = []
      total=0
      for items in resultDf["age_groups"].value_counts().iteritems():
       total=total+items[1]
      for items in resultDf["age_groups"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=self.changeAgeName(items[0])
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=negative_color
         mapBarsList.append(mapBar)


   
      title = "Low Engagement Level - Age Wise Split of Content Creators "

      mapSplineList=[]
      self.insertBasicColumnWithDetails(title,mapBarsList,mapSplineList,study_id,country,question_order_number, order_number ,self.ageFilter,self.genderFilter,self.engagementFilter,size_half,positive_color)
         

   """def getHighEngagementGenderAgeSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      resultDf = self.df.loc[self.df['engagement'] == 'high']
      mapBarsList = []
      total=0
      for items in resultDf["age_groups"].value_counts().iteritems():
       total=total+items[1]
      
      for items in resultDf["age_groups"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=items[0]
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=positive_color
         mapBarsList.append(mapBar)


         jsonInst= {}
      jsonInst["Title"] = "Overall High Engagement - Age Group of Content Creators"
      jsonInst["bar_values"]=mapBarsList
      #print(mapBarsList)
      jsonInst["spline_values"]=[]
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst ,self.ageFilter,self.genderFilter,self.engagementFilter)
      

   def getLowEngagementGenderAgeSplit(self):
      pass"""


   def getSection(self,study_id,country,question_order_number,order_number,product_type,value,value2,filename):
          sectionInst = section.SectionData()
          sectionInst.insertSection(study_id,country,question_order_number, order_number,product_type,value,value2,self.ageFilter,self.genderFilter,self.engagementFilter)

                   

  
   
def generateCharts():

       
      for filename in glob.glob("data/*.csv"):
             

         study_id,product_title= StudyGenerate.generateStudy(filename)
        

         
       

         f = open('started.out','a')
         f.write(str(study_id)+","+filename+","+filename.split("/")[1].split(".")[0]+".csv") 
         f.write("\n")
         
         country ="US"
         product_type="study_country_question"
         question_order_number=1

         filename = filename.split("/")[1].split(".")[0]
         
         #study_id=filenamesMap[filename]
         
         
         analyser = Analyser(filename)
         #print("Product is ___________" +filename)


         engagement_list = [None,'h', 'l']
         gender_list= [None, 'm', 'f']
         age_list = [None,1, 2, 3, 4 ]


         engagement_map = {"None":"",'h': 'High Engagement', 'l': 'Low Engagement'}
         gender_map = {"None":"",'m':'Male', 'f': 'Female'}
         age_map = {"None":"","1": 'below 20 years', "2": '21 - 25 years', "3": '25 - 30 years', "4": '31 years above' }
         db.order_number=0
         
         calculate_tfs_nfs.setProduct(filename)


         for engagement in engagement_list:
            for gender in gender_list:
                     for age in age_list:
                        analyser.engagementFilter=engagement
                        analyser.genderFilter=gender
                        analyser.ageFilter=age
                        titleArray=[engagement_map[str(analyser.engagementFilter)],gender_map[str(analyser.genderFilter)],age_map[str(analyser.ageFilter)]]

                        analyser.titlePostString=' , '.join(filter(None, titleArray))
                        
                        if not analyser.titlePostString == "":
                               analyser.titlePostString = " - "+analyser.titlePostString
                               
                               
        
                        #print("..................")
                        ##print(engagement,gender,age)
                        analyser.getSection(study_id,country,question_order_number,db.order_number,product_type,"Overall Attributes Generated From Video Content Analysis ","overallattributes",filename)
                        #analyser.getProductNFS(study_id,country,question_order_number,db.order_number,product_type)
                        

                        analyser.getOverallAttributes(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getOverallLikability(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getOverallDislikes(study_id,country,question_order_number,db.order_number,product_type,filename)
                        groupAttributes = analyser.getGroupAttributes(filename)
                        analyser.getSection(study_id,country,question_order_number,db.order_number,product_type,"BREAKOUT","breakout",filename)
                        if groupAttributes:
                           analyser.getBreakoutsForAttributeLikedislike(study_id,country,question_order_number,db.order_number,product_type,filename , groupAttributes)
                        #analyser.getBreakoutsForAttributeDisliked(study_id,country,question_order_number,db.order_number,product_type,filename , groupAttributes)
                        analyser.getSection(study_id,country,question_order_number,db.order_number,product_type,"Overall and Demographics Distinguishing High Engagement Content from Low Engagement Content For "+str(product_title).title(),"engagement",filename,)
                        analyser.getHighEngagmentOverall(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getLowEngagementOverall(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getGenderSplitEngagement(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getAgeGroupContentCreator(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getHighEngagementGenderSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getLowEngagementGenderSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getHighEngagementAgeSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
                        analyser.getLowEngagementAgeSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
                        #analyser.getHighEngagementGenderAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)
                        #analyser.getLowEngagementGenderAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)`
   
"""
analyser.engagementFilter=None
analyser.genderFilter=None
analyser.ageFilter=None
analyser.titlePostString=engagement_map[str(analyser.engagementFilter)] +" "+gender_map[str(analyser.genderFilter)]+" "+ age_map[str(analyser.ageFilter)]
#print("..................")
##print(engagement,gender,age)
groupAttributes = analyser.getGroupAttributes(filename)
#print(groupAttributes)
analyser.getOverallAttributes(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getBreakoutsForAttributeLikedislike(study_id,country,question_order_number,db.order_number,product_type,filename , groupAttributes)

analyser.getSection(study_id,country,question_order_number,db.order_number,product_type,"Overall Attributes Generated From Video Content Analysis ","overallattributes",filename)
#analyser.getProductNFS(study_id,country,question_order_number,db.order_number,product_type)

analyser.getOverallAttributes(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getOverallLikability(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getOverallDislikes(study_id,country,question_order_number,db.order_number,product_type,filename)
groupAttributes = analyser.getGroupAttributes(filename)
analyser.getBreakoutsForAttributeLikedislike(study_id,country,question_order_number,db.order_number,product_type,filename , groupAttributes)
#analyser.getBreakoutsForAttributeDisliked(study_id,country,question_order_number,db.order_number,product_type,filename , groupAttributes)
analyser.getSection(study_id,country,question_order_number,db.order_number,product_type,"Engagement","engagement",filename,)
analyser.getHighEngagmentOverall(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getLowEngagementOverall(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getGenderSplitEngagement(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getAgeGroupContentCreator(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getHighEngagementGenderSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getLowEngagementGenderSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getHighEngagementAgeSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
analyser.getLowEngagementAgeSplit(study_id,country,question_order_number,db.order_number,product_type,filename)
#analyser.getHighEngagementGenderAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)
#analyser.getLowEngagementGenderAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)
"""


if __name__ == "__main__":
   generateCharts()
   '''df = pd.read_csv("lipstick_sample"+".csv")


   engagement_list = [None,'h', 'l']
   gender_list= [None, 'm', 'f']
   age_list = [None,'below 20', '21-25', '25-30', '31 above' ]
   #print(".....................")
   resultDf=  tfs_service.get_group_attribute_data(df,engagement="l",age=None,gender="f")
   #print(resultDf)'''
   sys.exit(0)



       
                        
