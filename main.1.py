import pandas as pd
import json
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import re
from charts import pieChart
from charts import columnChart
from charts import verbatims
from charts import insight
from charts import html
from charts import columnChartMultiple
from  tfs_calculation import tfs_service

import copy
import math
import sys


negative_color = "#f6af42"
positive_color ="#50ADE8"


male_color = "#50ADE8"
female_color ="#f6af42"
class Analyser:
   df=None
   db=None
       
   def __init__(self, df):
      self.df = df

          

   def getProductNFS(self,study_id,country,question_order_number,order_number,product_type):
      htmlInstance = html.HTMLData()
      data={}
      value=10
      print("HTML Insert")
      htmlInstance.insertHTMLChart(study_id,country,question_order_number, order_number,value)

   def getOverallAttributes(self,study_id,country,question_order_number,order_number,product_type,file_name):
      self.df = pd.read_csv(file_name+".csv")
      resultDf=  tfs_service.get_group_attribute_data(self.df)

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_tfs","video_total_count","OVER")
      jsonInst= {}
      jsonInst["Title"] = "Overall Group attributes"
      jsonInst["bar_values"]=mapBarsList
      jsonInst["spline_values"]=mapSplineList
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)
      print("overall done")

      
   def createBarAndSplineLists(self,resultDf,dataPointName,barValueName,splineValueName,videoCountName,flag):


      rowSize = len(resultDf.index)
      columnSize = int(int(str(resultDf.size))/rowSize)
      numberOfVideos =  len(self.df)

      seriesValue=[]

      mapBarsList = []
      mapSplineList=[]
      

      data=[]
    

      
      for index, row in resultDf.iterrows():
         print(index)
         record=dict()
         record[dataPointName] = row[dataPointName]

         record[barValueName] =int(row[videoCountName]/numberOfVideos*100)

         record[splineValueName] =float(row[splineValueName])

         
         
         mapBar=dict()
         mapBar["name"]=record[dataPointName]
         mapBar["y"]=round(record[barValueName],1)

         if not mapBar["y"] == 0:
               
            if mapBar["y"] >= 0:
               mapBar["color"]=positive_color
            else:
               mapBar["y"]=mapBar["y"]*-1
               mapBar["color"]=negative_color
            mapBarsList.append(mapBar)


            mapSpline=dict()
            mapSpline["name"]=record[dataPointName]
            
            mapSpline["y"]=round(record[splineValueName],2)

            mapSpline["color"]=positive_color
            if flag  == "NEG" or  flag  == "OVER_NEG":
               mapBar["color"]=negative_color

            if flag  == "OVER_NEG":
               mapSpline["y"]=mapSpline["y"]*-1
            mapSplineList.append(mapSpline)

      return mapBarsList, mapSplineList
          

   def getOverallLikability(self,study_id,country,question_order_number,order_number,product_type,file_name):
      self.df = pd.read_csv(file_name+".csv")

      resultDf=  tfs_service.get_group_attribute_data(self.df)

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_positive_tfs","video_pos_count","POS")
      jsonInst= {}
      jsonInst["Title"] = "Positive Overall Group attributes"
      jsonInst["bar_values"]=mapBarsList
      jsonInst["spline_values"]=mapSplineList
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)
      print("overall done")

   def getOverallDislikes(self,study_id,country,question_order_number,order_number,product_type,file_name):
      self.df = pd.read_csv(file_name+".csv")

      resultDf=  tfs_service.get_group_attribute_data(self.df)
      print(resultDf.keys())

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_negative_tfs","video_neg_count","OVER_NEG")
      jsonInst= {} 
      jsonInst["Title"] = "Negative Overall Group attributes"
      jsonInst["bar_values"]=mapBarsList
      jsonInst["spline_values"]=mapSplineList
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)
      print("overall done")
      

   def getBreakoutsForAttributeLiked(self,study_id,country,question_order_number,order_number,product_type,file_name , groupAttributes):


      for ga in groupAttributes:
         print("for group attribute .......", ga)
         dfa = pd.read_csv(file_name+".csv")
         resultDf=  tfs_service.get_attribute_data(dfa,str(ga))
   
         mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"Attribute","Attribute_volumetric","Attribute_positive_tfs","video_pos_count","POS")
         if mapBarsList :
            jsonInst= {} 
            jsonInst["Title"] = "Positive breakout for attribute " +ga
            jsonInst["bar_values"]=mapBarsList
            jsonInst["spline_values"]=mapSplineList
            columnChartInstance = columnChart.ColumnChart()

            columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)

   def getBreakoutsForAttributeDisliked(self,study_id,country,question_order_number,order_number,product_type,file_name , groupAttributes):
         for ga in groupAttributes:
            print("for group attribute .......", ga)
            dfa = pd.read_csv(file_name+".csv")
            resultDf=  tfs_service.get_attribute_data(dfa,str(ga))
            
            mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"Attribute","Attribute_volumetric","Attribute_positive_tfs","video_pos_count","NEG")
            if mapBarsList :
               jsonInst= {} 
               jsonInst["Title"] = "Negative breakout for attribute " +ga
               jsonInst["bar_values"]=mapBarsList
               jsonInst["spline_values"]=mapSplineList
               columnChartInstance = columnChart.ColumnChart()

               columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)

   def getHighEngagmentOverall(self,study_id,country,question_order_number,order_number,product_type,file_name):
          
      self.df = pd.read_csv(file_name+".csv")

      resultDf=  tfs_service.get_group_attribute_data(self.df,engagement="h")

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_positive_tfs","video_pos_count","POS")
      jsonInst= {}
      jsonInst["Title"] = "High Engagement Overall Group attributes"
      jsonInst["bar_values"]=mapBarsList
      jsonInst["spline_values"]=mapSplineList
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)
      print("High Engagement overall done")

   def getLowEngagementOverall(self,study_id,country,question_order_number,order_number,product_type,file_name):
      self.df = pd.read_csv(file_name+".csv")

      resultDf=  tfs_service.get_group_attribute_data(self.df,engagement="l")

      mapBarsList,mapSplineList=self.createBarAndSplineLists(resultDf,"GroupAttribute","GroupAttribute_volumetric","GroupAttribute_positive_tfs","video_pos_count","NEG")
      jsonInst= {}
      jsonInst["Title"] = "Low Engagement Overall Group attributes"
      jsonInst["bar_values"]=mapBarsList
      jsonInst["spline_values"]=mapSplineList
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)
      print("Low Engagement overall done")

   def getGroupAttributes(self,file_name):
      
      self.df = pd.read_csv(file_name+".csv")
      resultDf=  tfs_service.get_group_attribute_data(self.df,"")
      rowSize = len(resultDf.index)
      columnSize = int(int(str(resultDf.size))/rowSize)
      numberOfVideos =  len(self.df)
      print("column size:", columnSize)
      print("rowSize:" ,rowSize)
      groupAttributesList=[]
      for i in range (1,rowSize-1):
         groupAttributesList.append(resultDf["GroupAttribute"][i])
      return  groupAttributesList
      

   def getGenderSplitEngagement(self,study_id,country,question_order_number,order_number,product_type,filename):
      result = self.df["gender"].value_counts()
      #print(self.df["age"].value_counts())
      #print(self.df["engagement"].value_counts())
      pieChartInstance = pieChart.PieCh()
      pieValueList = []      
      total = result["Male"] + result["Female"]
      pieValueM={}
      pieValueM["name"]="Male"
      pieValueM["y"]=result["Male"]*100/total
      pieValueM["color"]=male_color

      pieValueList.append(pieValueM)

      pieValueF={}
      pieValueF["name"]="Female"
      pieValueF["y"]=result["Female"]*100/total
      pieValueF["color"]=female_color
      
      pieValueList.append(pieValueF)

      jsonInst= {}
      jsonInst["Title"] = "Overall Engagement Gender Split of Content Creators"
      jsonInst["values"]=pieValueList

      

      pieChartInstance = pieChart.PieCh()

      pieChartInstance.insertPieChart(study_id,country,question_order_number, order_number,jsonInst)
      

   def getAgeGroupContentCreator(self,study_id,country,question_order_number,order_number,product_type,filename):
      mapBarsList = []
      total=0
      for items in self.df["age"].value_counts().iteritems():
       total=total+items[1]
      for items in self.df["age"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=items[0]
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=positive_color
         mapBarsList.append(mapBar)


         jsonInst= {}
      jsonInst["Title"] = "Overall Engagement - Age Group of Content Creators"
      jsonInst["bar_values"]=mapBarsList
      print(mapBarsList)
      jsonInst["spline_values"]=[]
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)
      

   def getLowEngagementGenderSplit(self,study_id,country,question_order_number,order_number,product_type,filename):

      resultDf = self.df.loc[self.df['engagement'] == 'low']
      pieValueList = []
      total=0
      for items in resultDf["gender"].value_counts().iteritems():
           total=total+items[1]
      for items in resultDf["gender"].value_counts().iteritems(): 

         pieValue=dict()
         pieValue["name"]=items[0]
         pieValue["y"]=round((int(items[1]*100)/int(total)))

         pieValueList.append(pieValue)


      jsonInst= {}
      jsonInst["Title"] = "Overall Low Engagement Gender Split of Content Creators"
      jsonInst["values"]=pieValueList

      pieChartInstance = pieChart.PieCh()

      pieChartInstance.insertPieChart(study_id,country,question_order_number, order_number,jsonInst)
      

   def getHighEngagementGenderSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      
      resultDf = self.df.loc[self.df['engagement'] == 'high']
      pieValueList = []
      total=0
      for items in resultDf["gender"].value_counts().iteritems():
           total=total+items[1]
      for items in resultDf["gender"].value_counts().iteritems(): 

         pieValue=dict()
         pieValue["name"]=items[0]
         pieValue["y"]=round((int(items[1]*100)/int(total)))

         pieValueList.append(pieValue)


      jsonInst= {}
      jsonInst["Title"] = "Overall High Engagement Gender Split of Content Creators"
      jsonInst["values"]=pieValueList

      pieChartInstance = pieChart.PieCh()

      pieChartInstance.insertPieChart(study_id,country,question_order_number, order_number,jsonInst)
      

   def getHighEngagementAgeSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      resultDf = self.df.loc[self.df['engagement'] == 'high']
      mapBarsList = []
      total=0
      for items in resultDf["age"].value_counts().iteritems():
       total=total+items[1]
      for items in resultDf["age"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=items[0]
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=positive_color
         mapBarsList.append(mapBar)


         jsonInst= {}
      jsonInst["Title"] = "Overall High Engagement - Age Group of Content Creators"
      jsonInst["bar_values"]=mapBarsList
      print(mapBarsList)
      jsonInst["spline_values"]=[]
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)

   def getLowEngagementAgeSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      resultDf = self.df.loc[self.df['engagement'] == 'low']
      mapBarsList = []
      total=0
      for items in resultDf["age"].value_counts().iteritems():
       total=total+items[1]
      for items in resultDf["age"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=items[0]
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=positive_color
         mapBarsList.append(mapBar)


         jsonInst= {}
      jsonInst["Title"] = "Overall Low Engagement - Age Group of Content Creators"
      jsonInst["bar_values"]=mapBarsList
      print(mapBarsList)
      jsonInst["spline_values"]=[]
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)
   
   def getHighEngagementGenderAgeSplit(self,study_id,country,question_order_number,order_number,product_type,filename):
      resultDf = self.df.loc[self.df['engagement'] == 'high']
      mapBarsList = []
      total=0
      for items in resultDf["age"].value_counts().iteritems():
       total=total+items[1]
      
      for items in resultDf["age"].value_counts().iteritems(): 

         mapBar=dict()
         mapBar["name"]=items[0]
         mapBar["y"]=round((int(items[1]*100)/int(total)))
         mapBar["color"]=positive_color
         mapBarsList.append(mapBar)


         jsonInst= {}
      jsonInst["Title"] = "Overall High Engagement - Age Group of Content Creators"
      jsonInst["bar_values"]=mapBarsList
      print(mapBarsList)
      jsonInst["spline_values"]=[]
      columnChartInstance = columnChart.ColumnChart()

      columnChartInstance.insertBasicColumn(study_id,country,question_order_number, order_number,jsonInst)

   def getLowEngagementGenderAgeSplit(self):
      pass


                

  
   
def generateCharts():
       

   
   study_id="5e16f31c59af31472dd59d9a"
   country ="IN"
   product_type="study_country_question"
   filename = "lipstick_sample"
   analyser = Analyser(filename)
   question_order_number=1
   order_number=1
   analyser.getProductNFS(study_id,country,question_order_number,order_number,product_type)
   analyser.getOverallAttributes(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getOverallLikability(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getOverallDislikes(study_id,country,question_order_number,order_number,product_type,filename)
   groupAttributes = analyser.getGroupAttributes(filename)
   analyser.getBreakoutsForAttributeLiked(study_id,country,question_order_number,order_number,product_type,filename , groupAttributes)
   analyser.getBreakoutsForAttributeDisliked(study_id,country,question_order_number,order_number,product_type,filename , groupAttributes)
   analyser.getHighEngagmentOverall(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getLowEngagementOverall(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getGenderSplitEngagement(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getAgeGroupContentCreator(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getLowEngagementGenderSplit(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getHighEngagementGenderSplit(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getHighEngagementAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)
   analyser.getLowEngagementAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)
   #analyser.getHighEngagementGenderAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)
   #analyser.getLowEngagementGenderAgeSplit(study_id,country,question_order_number,order_number,product_type,filename)


if __name__ == "__main__":
   generateCharts()
   sys.exit(0)
                        

