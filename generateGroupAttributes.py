import pandas as pd
import json
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
import re
from  tfs_calculation import tfs_service
from  tfs_calculation import calculate_tfs_nfs
from  tfs_calculation.tfs_service import filter_df
from  util import db
import copy
import math
import sys
import glob

   
def generateGroupAttributes():

      
      for filename in glob.glob("csv_in/*.csv"):
             
         filename = filename.split("/")[1].split(".")[0]
         
         

         calculate_tfs_nfs.setProduct(filename)

         df=pd.read_csv("csv_in/"+filename+".csv")
         
         for i in range(len(df)) : 

            sdf= df[i:i+1]
  
            resultDf = tfs_service.get_group_attribute_data(sdf)
            gaList=[]
            if not resultDf.empty:
               gaList=  list(resultDf["GroupAttribute"].T.to_dict().values())

            df.loc[df.index[i], 'group_attributes'] = str(gaList)
         df.to_csv("csv_out/"+filename+".csv")
            

def mergeDownloadAndGA():
    
      
      for filename in glob.glob("csv_out/*.csv"):
             
         filename = filename.split("/")[1].split(".")[0]
         

         df=pd.read_csv("csv_out/"+filename+".csv")
         df.insert(loc = 16,column= "sentiment", value=['' for i in range(df.shape[0])])
         for i in range(len(df)) : 
            
            sentiment = ""
            if df["sentiment_scores"].values[i] >= 0:
               sentiment = "Positive"
            if df["sentiment_scores"].values[i] < 0:
               sentiment = "Negative"
            df.loc[df.index[i], "sentiment"] = sentiment
            df.replace(r'\\n',' ', regex=True) 
            
         dfwithSelectedColumns = df[['age', 'age_groups', 'attributes', 'channel_name', 'comment_count', 'describers_nouns_pairs', 'describers_nouns_phrases', 'dislikes',
    'engagement', 'gender', 'likes', 'opinion_phrases', 'published_at', 
   'punctuated_transcription', 'sentiment_scores','sentiment', 'subscribers', 'suggestions', 'title', 'video_url', 'view_count', 'product','group_attributes']]
         dfwithSelectedColumns.to_csv("csv_ga_out_1/"+filename+".csv")

if __name__ == "__main__":
       
   # Generate group attributes on csvs having aspect sentiment
   generateGroupAttributes()
   
   # Clean columns for csv upload
   mergeDownloadAndGA()

   sys.exit(0)



       
                        
